EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_POT RV?
U 1 1 604E5EA8
P 1800 1875
F 0 "RV?" H 1731 1921 50  0000 R CNN
F 1 "R_POT" H 1731 1830 50  0000 R CNN
F 2 "" H 1800 1875 50  0001 C CNN
F 3 "~" H 1800 1875 50  0001 C CNN
	1    1800 1875
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 604E6251
P 2225 1600
F 0 "R?" V 2018 1600 50  0000 C CNN
F 1 "R" V 2109 1600 50  0000 C CNN
F 2 "" V 2155 1600 50  0001 C CNN
F 3 "~" H 2225 1600 50  0001 C CNN
	1    2225 1600
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 604E6BC4
P 3000 1575
F 0 "R?" V 2793 1575 50  0000 C CNN
F 1 "R" V 2884 1575 50  0000 C CNN
F 2 "" V 2930 1575 50  0001 C CNN
F 3 "~" H 3000 1575 50  0001 C CNN
	1    3000 1575
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 604E747A
P 2250 1875
F 0 "R?" V 2043 1875 50  0000 C CNN
F 1 "R" V 2134 1875 50  0000 C CNN
F 2 "" V 2180 1875 50  0001 C CNN
F 3 "~" H 2250 1875 50  0001 C CNN
	1    2250 1875
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 604E7BF9
P 2800 1975
F 0 "R?" H 2870 2021 50  0000 L CNN
F 1 "R" H 2870 1930 50  0000 L CNN
F 2 "" V 2730 1975 50  0001 C CNN
F 3 "~" H 2800 1975 50  0001 C CNN
	1    2800 1975
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q?
U 1 1 604E8A37
P 2600 2725
F 0 "Q?" H 2806 2771 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 2806 2680 50  0000 L CNN
F 2 "" H 2800 2825 50  0001 C CNN
F 3 "~" H 2600 2725 50  0001 C CNN
	1    2600 2725
	1    0    0    -1  
$EndComp
$EndSCHEMATC
